# code_playground

This is a personal coding playground. Code samples are added for fun and learning.
Most samples should compile using a standard cmake compilation with no special libraries
The project uses cmake for out of source building.

### Make the build environment
`$ cd code_playground`

`$ mkdir build`

`$ cd build`

### Select project to build in cmake:
`$ ccmake ../`

`build_eigen_playground -> on`

`build_pcl_playround -> on`

`build_playground -> on`

`c to configure & g to generate make file and exit`

### Make project
`$ make -j4`


