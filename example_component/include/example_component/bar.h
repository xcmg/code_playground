//#pragma once

#include <iostream>
#include "example_library/foo.h"

namespace example {

class Bar {
public:
    Bar();
    ~Bar();

    std::string helloWorld();
    
}; // Bar
} // namespace example