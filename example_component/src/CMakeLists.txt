file(GLOB SRCS bar.cpp)

set(example_component_include_dirs
    ${CMAKE_SOURCE_DIR}/libraries/example_library/include
)

set(example_component_include_libs
    example_library
)

build_executable(example_component example example_main.cpp ${SRCS})