#include "example_component/bar.h"

namespace example {
Bar::Bar() {
}

Bar::~Bar() {
}

std::string Bar::helloWorld() {
    example::Foo f;
    return f.helloWorld();
}

} // namespace example