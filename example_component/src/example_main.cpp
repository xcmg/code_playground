#include <iostream>
#include "example_component/bar.h"

int main(int argc, char** argv){

    example::Bar b;
    std::cout << b.helloWorld() << std::endl;
   
    return 0;
}