#pragma once 
#include <iostream>
#include <eigen3/Eigen/Dense>

namespace example {

class Foo {

public: 
    Foo();
    ~Foo();

    std::string helloWorld();

}; // Foo
}  // namespace example