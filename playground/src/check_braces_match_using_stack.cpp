#include <iostream>
#include <stack>

static const std::string open("([{");
static const std::string close(")]}");

int main(int argc, char** argv){

    const std::string input("((({{}}))[]"); // Good
    //const std::string input("((({{}})[");   // Bad
    
    std::stack<char> lifo;
    for(unsigned int i = 0; i < input.length(); ++ i){

        //If its open brace, put on stack
        if(open.find(input[i]) != std::string::npos){
            lifo.push(input[i]);
        } else {
            const auto p = close.find(input[i]);
            
            // Check if its a close brace, if true check its got
            // the matching index as the open, 
            if(p != std::string::npos){
                if(p == open.find(lifo.top())){
                    lifo.pop();
                } else {
                    break;
                }          
            }
        }
    }

    // If good, lifi is empty, otherwise not 
    if(lifo.size() > 0)
        std::cout << "!!! Braces miss match !!!" << std::endl;
    else
        std::cout << "Braces match :-)" << std::endl;
    
    return 0;
}
