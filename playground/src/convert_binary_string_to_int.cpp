#include <iostream>
#include <string>

int main(int argc, char** argv){

    const std::string bstr("10011001");
    std::cout << "Binary Input: " << bstr << std::endl;

    // Built in converter
    int value = std::stoi(bstr, nullptr, 2);
    std::cout << value << std::endl;

    // Using binary operators
    int value2 = 0;
    for(unsigned int i=0; i < bstr.length(); ++i){
        if(bstr[i] == '1')
            value2 |= (1<<i);
    }
    std::cout << value2 << std::endl;

    return 0;
}
