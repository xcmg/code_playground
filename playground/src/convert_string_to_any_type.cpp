#include <iostream>

// Get value as an int
template<typename T>
T getValue(std::string value){
    return std::stoi(value, nullptr, 10);
}

// Overload get value as int with different base
template<typename T>
T getValue(std::string value, int base){
    return std::stoi(value, nullptr, base);
}

// Specialisation, for double
template<>
double getValue(std::string value) {
    return std::stod(value);
}

int main(int argc, char** argv){

    int i = getValue<int>("123");
    std::cout << i << std::endl;

    int h = getValue<int>("0xA",16);
    std::cout << h << std::endl;

    double d = getValue<double>("123.4");
    std::cout << d << std::endl;
    
    return 0;
}