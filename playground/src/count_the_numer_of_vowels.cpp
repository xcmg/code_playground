#include <iostream>
#include <algorithm>

static const std::string vowels("aeiou");

int main(int argc, char** argv) {

    std::string str("Roses are red, violets are blue");
    int num =0;

    std::for_each(str.begin(), str.end(), 
        [&](const char &c){ if(vowels.find(c) != std::string::npos) ++num;});

    std::cout << "There are " << num 
              << " of vowels in the following string: " 
              << str << std::endl;

    return 0;
}