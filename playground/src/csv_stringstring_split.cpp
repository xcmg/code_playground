#include <iostream>
#include <sstream>
#include <vector>

std::vector<std::string> split(const std::string in, const char deliminator){
    
    std::vector<std::string> parts;
    std::istringstream ss(in);
    std::string part;

    while(getline(ss, part, deliminator)){
        parts.push_back(part);
    }
    
    return parts;
} 

int main(int argc, char** argv){

    const std::string str("abc,def,ghi,jkl");
    const auto parts = split(str, ',');

    for(const auto s : parts)
        std::cout << s << std::endl;

    return 0;
}

