#include <iostream>
#include <map>

int main(int argc, char** argv) {

    const std::string str = "ddhdjsdhjka";
    std::map<char, int> table;

    // Iterate through the string, increment on each occurrance 
    for(const auto &c : str){
        const auto key = table.find(c);
        if(key == table.end()){
            table.emplace(std::make_pair(c, 1));
        } else {
            key->second++;
        }
    }

    // Map doesn't keep order, so go through string, find first and break
    std::string first_unique;
    for(const auto &c : str) {
        const auto key = table.find(c);
        if(key != table.end() && key->second == 1){
            first_unique = c; 
            break;
        }
    }

    std::cout << "First unique letter in: " << str << " is: " << first_unique << std::endl;
    return 0;
}
