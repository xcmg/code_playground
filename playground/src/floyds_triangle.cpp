#include <iostream>
#include <iomanip>

int main(int argc, char** argv) {

    if(argc == 2) {

        std::string input(argv[1]);
        const size_t levels = std::stoi(input, nullptr, 10);
        size_t count = 1;

        for(size_t i = 0; i < levels; ++i){
            for(size_t j = 0; j <= i; ++j){
                std::cout << std::setw(4) << count;
                ++count;
            }
            std::cout << std::endl;
        }
    } else {
        std::cout << " Specify No. levels" << std::endl;
        std::cout << " e.g. ./bin/floyds_triangle 4" << std::endl;
    }

    return 0;
}