#include <iostream>

template <typename T>
T my_max(T a, T b) {
    return (a > b ? a : b);
}

int main(int argc, char** argv) {

    double a = 10;
    double b = 12;
    double c = my_max(a,b);

    std::cout << c << std::endl;

    return 0;
}