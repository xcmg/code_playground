#include <iostream>
#include <algorithm>


static const std::string permitted("({[<>]})"); 

int main(int argc, char** argv){

    std::string input("jj()kjfjk<>jkdj[]{}");
    std::cout << "Original: " << input << std::endl;

    // Lambda expression
    const auto lambda = [&](const char &a) -> bool {
        return permitted.find(a) == std::string::npos;
    };

    // Predicate feeding into erase
    input.erase( std::remove_if(input.begin(), input.end(), lambda), input.end());

    std::cout << "Modified: " << input << std::endl;

    return 0;
}
