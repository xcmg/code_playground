#include <iostream>

int main(int argc, char** argv) {

    int number = 12345;
    int number_rev = 0;

    std::cout << "Original Number: " << number << std::endl;

    while(number != 0){

        number_rev *= 10;
        number_rev += number % 10;
        number /= 10;
    };

    std::cout << "Reversed Number: " << number_rev << std::endl;

    return 0;
}
