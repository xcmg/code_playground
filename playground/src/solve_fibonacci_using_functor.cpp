#include <iostream>

// Functoor classes is an object which acts
// like a function or a function pointer.
// In this case the functor holds a finonacci 
// state between iteratve calls.

class fibonacci {
public:
    fibonacci(int number) :
        n_{number},
        it_{0},
        first_{1},
        second_{1} {
    }

    bool operator() () {
        if(n_ != it_){
            int next = first_ + second_;
            first_ = second_;
            second_ = next;
            it_ ++;
        }

        return n_ == it_;
    }

    int n_;
    int it_;
    int first_;
    int second_;
};

int main(int argc, char** argv){

    int n = 5;
    fibonacci fib(n);

    while ( !fib() );
    
    std::cout << n << "th finonacci number is: " << fib.second_ << std::endl;

    return 0;
}