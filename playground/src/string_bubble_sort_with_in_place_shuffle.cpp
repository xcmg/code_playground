#include <iostream>

int main(int argc, char** argv){

    std::string str("jdas43fdfd");
    std::cout << "Random String: " << str << std::endl;

    for(unsigned int i=0; i < str.length(); ++i) {
        for(unsigned int j=i; j<str.length(); ++j) {
            if(str[i] < str[j]){

                //Built in swap
                //std::swap(str[i], str[j]);

                // Magix Xor swap
                str[i] ^= str[j];
                str[j] ^= str[i];
                str[i] ^= str[j];
            }
        }
    }

    std::cout << "Sorted: " << str << std::endl; 

    return 0;
}
