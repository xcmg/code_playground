#include <iostream>
#include <algorithm>

int main(int argc, char** argv){

    std::string str("Random String");

    for(auto &c : str)
        c = std::toupper(c);

    std::cout << "ToLower: " << str << std::endl;

    std::for_each(str.begin(), str.end(),
                  [](char &c){c = std::tolower(c);});

    std::cout << "ToUpper: " << str << std::endl;

    return 0;
}