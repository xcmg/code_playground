#include <iostream>
#include <vector>
#include <functional>
#include <algorithm>

int main(int argc, char** argv){

    std::vector<int> first = {3,2,8,4};
    std::vector<int> second = {1,2,3,4};
    std::vector<int> results;

    std::transform(first.begin(), first.end(),
                   second.begin(), 
                   back_inserter(results),
                   std::minus<int>() );

    for( const auto c : results)
        std::cout << c << std::endl;

    return 0;
}