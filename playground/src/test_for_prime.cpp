#include <iostream>
#include <math.h>

int main(int argc, char** argv){

    //Test number between 0 & 500
    for(int input = 1; input <= 500; ++input) {

        // Prime cannot be 1 or divisable by 2 except 2
        if( input == 1 || (input % 2 == 0 && input != 2) ) 
            continue;

        // Prime cannot be divisiable by 5 except 5
        if( (input % 5 == 0 && input != 5) )
            continue;

        // Check if divisable, only need to check half the range
        bool prime = true;
        int input_half = input/2;

        for(int test = 3; test <= input_half; ++test){
            if(input % test == 0){
               prime = false;
               break;
            }
        }

        if(prime)
            std::cout << input << " is a prime" << std::endl;

    }

    return 0;
}