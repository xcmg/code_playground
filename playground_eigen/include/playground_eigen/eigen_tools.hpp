#pragma once

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Geometry>
#include <eigen3/Eigen/Dense>
#include <iostream>
#include <chrono>
#include <ratio>
#include <thread>
#include <stdint.h>

namespace playground_eigen {

/*
 * Rotates a 2D point x,y about a single angle in yaw
 */
Eigen::Vector2d transform2D(const double &dx, const double &dy, const double &daz){

	Eigen::Vector2d point;
	point(0) = dx;
	point(1) = dy;
	Eigen::Rotation2D<double> r(daz);
	
	return (r.toRotationMatrix() * point);
}

/*
 * Creates an affine 2D matrix with rotation and translation
 */
Eigen::Matrix3f create_affine_matrix_2D(const double &dx, const double &dy, const double &daz){

	Eigen::Transform<float, 2, Eigen::Affine> t;
	Eigen::Vector2f trans(dx, dy);
	t = Eigen::Translation<float, 2>(trans);
	t.rotate(Eigen::Rotation2D<float>(daz));

	return t.matrix();
}

/*
 * Creates a 3D matrix with translation and rotation around 6dof.
 * This uses the raw maths
 */
Eigen::Matrix4d createTransformation(const double &dx, const double &dy, const double &dz, const double &dax, const double &day, const double &daz) {
	Eigen::Matrix4d pose(4,4);
	const double cr = cos(dax);
	const double cp = cos(day);
	const double cq = cos(daz);
	const double sr = sin(dax);
	const double sp = sin(day);
	const double sq = sin(daz);
	pose(0, 0) = cp * cq;
	pose(0, 1) = -cr*sq+sr*sp*cq;
	pose(0, 2) = sr*sq + cr*sp*cq;
	pose(1, 0) = cp*sq;
	pose(1, 1) = cr*cq + sr*sp*sq;
	pose(1, 2) = -sr*cq + cr*sp*sq;
	pose(2, 0) = -sp;
	pose(2, 1) = sr*cp;
	pose(2, 2) = cr*cp;
	pose(0, 3) = dx;
	pose(1, 3) = dy;
	pose(2, 3) = dz;
	pose(3, 3) = 1;

	return pose;
}

/*
 * https://stackoverflow.com/questions/25504397/eigen-combine-rotation-and-translation-into-one-matrix
 * Creates an affine rotation matrix
 */
Eigen::Affine3d create_rotation_matrix(double ax, double ay, double az) {
  Eigen::Affine3d rx = Eigen::Affine3d(Eigen::AngleAxisd(ax, Eigen::Vector3d(1, 0, 0)));
  Eigen::Affine3d ry = Eigen::Affine3d(Eigen::AngleAxisd(ay, Eigen::Vector3d(0, 1, 0)));
  Eigen::Affine3d rz = Eigen::Affine3d(Eigen::AngleAxisd(az, Eigen::Vector3d(0, 0, 1)));

  return rz * ry * rx;
}

/*
 * Creates a 3D matrix with translation and rotation around 6dof.
 * This uses the a translation and rotation API
 */
Eigen::Matrix4d createTransform2(const double &dx, const double &dy, const double &dz, const double &dax, const double &day, const double &daz){
	Eigen::Affine3d r = create_rotation_matrix(dax, day, daz);
	Eigen::Affine3d t(Eigen::Translation3d(Eigen::Vector3d(dx,dy,dz)));
	Eigen::Matrix4d m = (t * r).matrix(); 	// Option 1
//	Eigen::Matrix4d m = t.matrix(); 		// Option 2
//	m *= r.matrix();

	return m;
}

Eigen::Matrix4d createTransform3(const double &dx, const double &dy, const double &dz, const double &dax, const double &day, const double &daz){

	Eigen::Affine3d t(Eigen::Translation3d(Eigen::Vector3d(dx,dy,dz)));
	t *= Eigen::AngleAxisd(daz, Eigen::Vector3d(0, 0, 1));
	t *= Eigen::AngleAxisd(day, Eigen::Vector3d(0, 1, 0));
	t *= Eigen::AngleAxisd(dax, Eigen::Vector3d(1, 0, 0));

	return t.matrix();
}


Eigen::Matrix4f create_affine_matrix(const double &dx, const double &dy, const double &dz, const double &dax, const double &day, const double &daz)
{
    Eigen::Transform<float, 3, Eigen::Affine> t;
    Eigen::Vector3f trans(dx, dy, dz);
    t = Eigen::Translation<float, 3>(trans);
    t.rotate(Eigen::AngleAxis<float>(dax, Eigen::Vector3f::UnitX()));
    t.rotate(Eigen::AngleAxis<float>(day, Eigen::Vector3f::UnitY()));
    t.rotate(Eigen::AngleAxis<float>(daz, Eigen::Vector3f::UnitZ()));

    return t.matrix();
}

Eigen::Affine3d create_affine_matrix(const double &dx, const double &dy, const double &dz, const double &daz)
{
	Eigen::Affine3d m = Eigen::Affine3d::Identity();
	m *= Eigen::Translation3d(dx, dy, dz);
	m *= Eigen::AngleAxis<double>(daz, Eigen::Vector3d::UnitZ());


//    Transform<float, 3, Eigen::Affine> t;
//    t = AngleAxis<float>(c, Vector3f::UnitZ());
//    t.prerotate(AngleAxis<float>(b, Vector3f::UnitY()));
//    t.prerotate(AngleAxis<float>(a, Vector3f::UnitX()));
//    t.pretranslate(trans);
//    return t.matrix();

	return m;
}



void createTransform()
{

	
//	Eigen::Vector2d first( 3, 3);
//	Eigen::Vector2d second( 6, 6);
//	Eigen::Vector2d cpoints(  (second-first)  + first );
//
//	std::cout << "first: " << first(0) << "," << first(1) << std::endl;
//	std::cout << "second: " << second(0) << "," << second(1) << std::endl;
//	std::cout << "cpoint: " << cpoints(0) << "," << cpoints(1) << std::endl;
//
//	Eigen::Vector2d penult( 9, 9);
//	Eigen::Vector2d last( 12, 12);
//	Eigen::Vector2d cpointe( (last - penult) + last );
//
//	std::cout << "penult: " << penult(0) << "," << penult(1) << std::endl;
//	std::cout << "last: " << last(0) << "," << last(1) << std::endl;
//	std::cout << "cpoint: " << cpointe(0) << "," << cpointe(1) << std::endl;



//		double distance = 2;
//
//		std::vector<double> angles;
//		for(double angle = 0; angle <= 1.17; angle += 0.25) {
//			angles.push_back(angle);
//			if (angle != 0) {
//				angles.push_back(-angle);
//			}
//		}
//
//		const Eigen::Vector2d priorPoint{-distance, 0};
//		for(const auto &currentAngle : angles) {
//			const Eigen::Vector2d origin{0, 0};
//			const Eigen::Vector2d next{distance * cos(currentAngle), distance * sin(currentAngle)};
//			for(const auto &postAngle : angles) {
//				const Eigen::Vector2d postPoint = next + Eigen::Vector2d{distance * cos(postAngle), distance * sin(postAngle)};
//
////				std::cout << "current=" << currentAngle << " post" << postAngle << std::endl;
////				std::cout << "prior=" << priorPoint(0) << "," << priorPoint(1) << std::endl;
////				std::cout << "origin=" << std::endl  << origin << std::endl;
////				std::cout << "next=" << std::endl << next << std::endl;
////				std::cout << "post=" << std::endl << postPoint << std::endl;
//
//				std::cout << currentAngle << "," << postAngle << ","
//						  << priorPoint(0) << "," << priorPoint(1) << ","
//						  << origin(0) << "," << origin(1) << ","
//						  << next(0) << "," << next(1) << ","
//						  << postPoint(0) << "," << postPoint(1) << std::endl;
//			}
//		}




//	// Origin and current position
//	Eigen::Vector3d o(0, 0 ,0);
//	Eigen::Vector3d p(0, 5, 0);
//
//
//	// Vehicle Position affine
//	Eigen::Affine3d vp = create_affine_matrix( p(0) - o(0), p(1) - o(1), 0, -M_PI/2);
//	std::cout << std::fixed << vp.matrix() << std::endl << std::endl;
//
//	// Sensor position
//	Eigen::Affine3d sp = create_affine_matrix(1,1,0,0);
//	std::cout << std::fixed << sp.matrix() << std::endl << std::endl;
//
//
//
//	auto t = sp.translation();
//	auto r = sp.rotation();
//	Eigen::Vector4d pos = Eigen::Vector4d(t(0),t(1),t(2),1.0);
//	std::cout << "Vector from affine before translation and rotation" << std::endl;
//	std::cout << std::fixed << pos << std::endl << std::endl;
//	std::cout << std::fixed << r << std::endl << std::endl;
//
//	sp = vp * sp;
//
//	auto t2 = sp.translation();
//	auto r2 = sp.rotation();
//	Eigen::Vector4d pos2 = Eigen::Vector4d(t2(0),t2(1),t2(2),1.0);
//	std::cout << "Vector from affine after translation and rotation" << std::endl;
//	std::cout << std::fixed << pos2 << std::endl << std::endl;
//	std::cout << std::fixed << r2 << std::endl << std::endl;




}

} // playground_eigen
