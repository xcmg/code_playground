#include <iostream>
#include <eigen3/Eigen/Dense>
#include "playground_eigen/eigen_tools.hpp"

int main(int argc, char** argv) {

    // Create a transfrom matrix 
    const double dx = 10, dy = 11, dz = 12;
    const double dax = 0.5, day= 1.0, daz = 1.5;  

    std::cout << "x,y,z: " << dx << " " << dy << " " << dz << std::endl;
    std::cout << "r,p,y: " << dax << " " << day << " " << daz << std::endl << std::endl;

    const auto mat = playground_eigen::createTransform2(dx, dy, dz, dax, day, daz);
    std::cout << "Matrix: \n" << mat << std::endl << std::endl;

    // Convert to an affine
    Eigen::Affine3d transform;
    transform.matrix() = mat;

    std::cout << "Translation: " << transform.translation().transpose() << std::endl;
    std::cout << "Rotation: " << transform.rotation().eulerAngles(2,1,0).transpose().reverse() << std::endl;

    return 0;
}