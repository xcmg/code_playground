#include <iostream>
#include <eigen3/Eigen/Dense>
#include <random>
using namespace std;
using namespace Eigen;

double frand(const double &min, const double &max){
    double t = (double)rand() / RAND_MAX;
    return min + t * (max - min);
}

Eigen::MatrixXd GetSpherePoints(){

    const double x = 0;
    const double y = -5;
    const double z = 10;
    const double radius = 10.0;
    const double min_inclination = 0;
    const double max_inclination = M_PI/2;
    const double min_azimuth = 0;
    const double max_azimuth = M_PI/2;

    const size_t max_points = 100;
    Eigen::MatrixXd m(max_points, 4);

    for(size_t i = 0; i < max_points; ++i){
        const double inclination  = frand(min_inclination, max_inclination);
        const double azimuth = frand(min_azimuth, max_azimuth);

        m(i,0) = x + radius * sin(inclination) * cos(azimuth);
        m(i,1) = y + radius * sin(inclination) * sin(azimuth);
        m(i,2) = z + radius * cos(inclination);
        m(i,3) = 0;
    }

    return m;
}

//https://jekel.me/2015/Least-Squares-Sphere-Fit/
int main(int argc, char** argv) {

    Eigen::MatrixXd p = GetSpherePoints(); 
    //std::cout << "Here is the points P:\n" << p.block(0,0,p.rows(),3) << std::endl;

    Eigen::MatrixXd a = p * 2;
    a.col(3).setOnes();
    //std::cout << "Here is the points A:\n" << a << std::endl;

    Eigen::VectorXd f(p.rows());
    for(int i = 0; i < p.rows(); ++i){
        const double x = p(i,0);
        const double y = p(i,1);
        const double z = p(i,2);
        f(i) = (x*x) + (y*y) + (z*z);
    }
    //std::cout << "Here is the points F:\n" << f << std::endl;

    const Eigen::VectorXd c = a.bdcSvd(ComputeThinU | ComputeThinV).solve(f); 
    //cout << std::fixed << "The least-squares solution is:\n" << c << endl;
    

    const double t = (c(0)*c(0))+(c(1)*c(1))+(c(2)*c(2))+c(3);
    const double r = sqrt(t);

    std::cout << std::fixed << "Radius: " << r 
                            << " x:" << c(0) 
                            << " y:" << c(1) 
                            << " z:" << c(2) << std::endl;
}
