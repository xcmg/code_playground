#include <iostream>
#include <vector>
#include <eigen3/Eigen/Dense>

// https://forum.kde.org/viewtopic.php?f=74&t=110265
// PCA Finds the line origin (mid point) and eigen vector direction
// Data expected data to be row major
// x ... n
// y ... n
// z ... n
std::pair <Eigen::Vector3d, Eigen::Vector3d> 
bestLineFromPoints(Eigen::MatrixXd points){

    const Eigen::Vector3d centroid = points.rowwise().mean();
    const auto centred_points = points.colwise() - centroid;
    const auto covariance_matrix = centred_points.adjoint().transpose() * centred_points.transpose();

    Eigen::SelfAdjointEigenSolver<Eigen::MatrixXd> eig(covariance_matrix);
    const auto direction  = eig.eigenvectors().col(2).normalized();

    return {centroid, direction};
}


int main(int argc, char** argv) {

    const size_t count = 5;
    Eigen::MatrixXd line(3, count);
    
    for(size_t i = 0; i < count; ++i) {
        line(0,i) = static_cast<double>(i);
        line(1,i) = 0.0;
        line(2,i) = 0.0;
    } 
    
    const auto results = bestLineFromPoints(line);
    std::cout << "Origin \n" << std::get<0>(results) << std::endl;
    std::cout << "Direction \n" << std::get<1>(results) << std::endl;
}
