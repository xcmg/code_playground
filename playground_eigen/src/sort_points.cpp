#include <iostream>
#include <vector>
#include <eigen3/Eigen/Dense>


int main(int argc, char** argv) {

    // Square points
    Eigen::Vector3f p1(0, 0, 0);
    Eigen::Vector3f p2(0, 1, 0);
    Eigen::Vector3f p3(1, 1, 0);
    Eigen::Vector3f p4(1, 0, 0);

    // Random order
    std::vector<Eigen::Vector3f> points;
    points.emplace_back(p1);
    points.emplace_back(p3);
    points.emplace_back(p4);
    points.emplace_back(p2);

    // Calculate centroid and make a normal
    const auto c = (p1 + p2 +p3 +p4) / 4;
    Eigen::Vector3f cn(0,0,1);

    // Lambda sort puts points in clockwise ordering
    const auto lsort = [&c, &cn](const auto &a, const auto &b) -> bool {

        std::cout << "Comparing: \n" << "A: " << a.transpose() << "\n" << "B: " << b.transpose() << std::endl;
        const auto ac = a-c;
        const auto bc = b-c;
        const float result = cn.dot(ac.cross(bc));

        std::cout << "Result: " << result << " B is " << (result >= 0.0 ? "cw" : "ccw") << std::endl;
        return result <= 0.0;
    };

    // Sort providing comparison
    std::sort(points.begin(), points.end(), lsort);

    // Display results
    for(const auto &p : points){
        std::cout << "Sorted Point: \n" << p.transpose() << std::endl;
    }
}
