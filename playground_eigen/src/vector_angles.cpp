#include <iostream>
#include <math.h>
#include <eigen3/Eigen/Dense>
#include "playground_eigen/eigen_tools.hpp"

int main(int argc, char** argv){

    const double rad = M_PI/6.0;
    const double length = 10.0;

    // Creating Point 1 and 2 using trig to rotate a point
    const Eigen::Vector2d p1 = {0,0};
    const Eigen::Vector2d p2 = {p1(0) + length*std::cos(rad), p1(1) + length*std::sin(rad)};
    
    std::cout << std::fixed << "P1: " << p1.transpose() << std::endl;
    std::cout << std::fixed << "P2: " << p2.transpose() << std::endl;
    std::cout << std::fixed << "ROT: " << rad << std::endl;

    // Standard angle between two cartesian 2D points   
    double angle = std::atan2(p2(1) - p1(1), p2(0) - p1(0));
    std::cout << "Catesian atan2(dy,dx): " << angle << std::endl<< std::endl; 


    
    // Example using Eigen to calc rotation of a vector
    const Eigen::Vector2d v1 = {length,0};
    const Eigen::Vector2d v2 = playground_eigen::transform2D(v1(0), v2(1), rad);

    std::cout << std::fixed << "V1: " << v1.transpose() << std::endl;
    std::cout << std::fixed << "V2: " << v2.transpose() << std::endl;
    std::cout << std::fixed << "ROT: " << rad << std::endl;

    const double dot = v1(0) * v2(0) + v1(1) * v2(1);
    const double det = v1(0) * v2(1) - v1(1) * v2(0);
    angle = std::atan2(det,dot);  
    std::cout << "Vector atan2(det,dot): " << angle << std::endl;

    // Another way to do the same thing
    angle = atan2(v2(1), v2(0)) - atan2(v1(1), v1(0));
    std::cout << "Vector atan2(det,dot): " << angle << std::endl;


    //TODO:: For 3D Vectors

    return 0;
}
