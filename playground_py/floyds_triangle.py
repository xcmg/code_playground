#!/usr/bin/env python3
import sys

if __name__ == '__main__':
    
    levels = int(sys.argv[1])
    count = 0 

    for x in range(levels):
        for y in range(x):
            print('{:4}'.format(count), end='')
            count +=1
        print('')
